from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from api import views
from client.views import homepage, datagrid, ssreport

router = routers.DefaultRouter()
router.register(r'membership', views.MembershipViewSet)

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'tcnanywhere.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', homepage),
    url(r'^datagrid', datagrid),
    url(r'^ssreport', ssreport),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
)
