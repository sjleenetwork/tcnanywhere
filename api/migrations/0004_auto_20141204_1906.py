# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20141204_1843'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='families',
            options={'verbose_name_plural': 'Families', 'verbose_name': 'Family'},
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Address',
            field=models.CharField(db_column='Address', blank=True, verbose_name='Address', max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='City',
            field=models.CharField(db_column='City', blank=True, verbose_name='City', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Email',
            field=models.EmailField(db_column='Email', blank=True, verbose_name='Email', max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Phone',
            field=models.CharField(db_column='Phone', blank=True, verbose_name='Phone', max_length=25),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='State',
            field=models.CharField(db_column='State', blank=True, verbose_name='State', max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Zip',
            field=models.CharField(db_column='Zip', blank=True, verbose_name='Zip', max_length=10),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contacttypes',
            name='Description',
            field=models.CharField(db_column='Description', blank=True, verbose_name='Description', max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='families',
            name='Description',
            field=models.CharField(db_column='Description', blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='families',
            name='Surname',
            field=models.CharField(db_column='Surname', max_length=40),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='familyroles',
            name='Description',
            field=models.CharField(db_column='Description', blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='familyroles',
            name='Role',
            field=models.CharField(db_column='Role', max_length=40),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membership',
            name='Anniversary',
            field=models.DateField(db_column='Anniversary', blank=True, verbose_name='Anniversary'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membership',
            name='Birthday',
            field=models.DateField(db_column='Birthday', blank=True, verbose_name='Birthday'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membership',
            name='MiddleName',
            field=models.CharField(db_column='MiddleName', blank=True, verbose_name='Middle Name', max_length=40),
            preserve_default=True,
        ),
    ]
