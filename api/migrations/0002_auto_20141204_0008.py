# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='membershipcontactentries',
            name='ContactEntryId',
        ),
        migrations.RemoveField(
            model_name='membershipcontactentries',
            name='MembershipId',
        ),
        migrations.DeleteModel(
            name='MembershipContactEntries',
        ),
        migrations.AlterModelOptions(
            name='contactentries',
            options={'verbose_name_plural': 'Contact Entries'},
        ),
        migrations.AlterModelOptions(
            name='contacttypes',
            options={'verbose_name_plural': 'Contact Types'},
        ),
        migrations.AddField(
            model_name='contactentries',
            name='ContactTypeId',
            field=models.ForeignKey(default=0, verbose_name='ContactTypeId', to='api.ContactTypes', related_name='CEContactTypeId'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contactentries',
            name='MembershipId',
            field=models.ForeignKey(default=0, verbose_name='MembershipId', to='api.Membership', related_name='CEMembershipId'),
            preserve_default=False,
        ),
    ]
