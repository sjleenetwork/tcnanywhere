# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactEntries',
            fields=[
                ('Id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('Address', models.CharField(max_length=254, verbose_name='Address', db_column='Address')),
                ('City', models.CharField(max_length=100, verbose_name='City', db_column='City')),
                ('State', models.CharField(max_length=2, verbose_name='State', db_column='State')),
                ('Zip', models.CharField(max_length=10, verbose_name='Zip', db_column='Zip')),
                ('Phone', models.CharField(max_length=25, verbose_name='Phone', db_column='Phone')),
                ('Email', models.EmailField(max_length=254, verbose_name='Email', db_column='Email')),
            ],
            options={
                'db_table': 'ContactEntries',
                'verbose_name_plural': 'ContactEntries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactTypes',
            fields=[
                ('Id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('Type', models.CharField(max_length=30, verbose_name='Type', db_column='Type')),
                ('Description', models.CharField(max_length=254, verbose_name='Description', db_column='Description')),
            ],
            options={
                'db_table': 'ContactTypes',
                'verbose_name_plural': 'ContactTypes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Membership',
            fields=[
                ('Id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('LastName', models.CharField(max_length=40, verbose_name='Last Name', db_column='LastName')),
                ('FirstName', models.CharField(max_length=40, verbose_name='First Name', db_column='FirstName')),
                ('MiddleName', models.CharField(max_length=40, verbose_name='Middle Name', db_column='MiddleName')),
                ('Birthday', models.DateField(verbose_name='Birthday', db_column='Birthday')),
                ('Anniversary', models.DateField(verbose_name='Anniversary', db_column='Anniversary')),
            ],
            options={
                'db_table': 'Membership',
                'verbose_name_plural': 'Membership',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MembershipContactEntries',
            fields=[
                ('Id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('ContactEntryId', models.ForeignKey(verbose_name='ContactEntryId', to='api.ContactEntries', related_name='MCEContactEntryId')),
                ('MembershipId', models.ForeignKey(verbose_name='ContactTypeId', to='api.ContactTypes', related_name='MCEContactTypeId')),
            ],
            options={
                'db_table': 'MembershipContactEntries',
                'verbose_name_plural': 'MembershipContactEntries',
            },
            bases=(models.Model,),
        ),
    ]
