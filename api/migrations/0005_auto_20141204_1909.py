# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20141204_1906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactentries',
            name='Address',
            field=models.CharField(db_column='Address', null=True, verbose_name='Address', blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='City',
            field=models.CharField(db_column='City', null=True, verbose_name='City', blank=True, max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Email',
            field=models.EmailField(db_column='Email', null=True, verbose_name='Email', blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Phone',
            field=models.CharField(db_column='Phone', null=True, verbose_name='Phone', blank=True, max_length=25),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='State',
            field=models.CharField(db_column='State', null=True, verbose_name='State', blank=True, max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='Zip',
            field=models.CharField(db_column='Zip', null=True, verbose_name='Zip', blank=True, max_length=10),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contacttypes',
            name='Description',
            field=models.CharField(db_column='Description', null=True, verbose_name='Description', blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='families',
            name='Description',
            field=models.CharField(db_column='Description', null=True, blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='familyroles',
            name='Description',
            field=models.CharField(db_column='Description', null=True, blank=True, max_length=254),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membership',
            name='Anniversary',
            field=models.DateField(db_column='Anniversary', null=True, verbose_name='Anniversary', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membership',
            name='Birthday',
            field=models.DateField(db_column='Birthday', null=True, verbose_name='Birthday', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='membership',
            name='MiddleName',
            field=models.CharField(db_column='MiddleName', null=True, verbose_name='Middle Name', blank=True, max_length=40),
            preserve_default=True,
        ),
    ]
