# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_auto_20141204_1909'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupMembers',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'GroupMembers',
                'verbose_name': 'Group Member',
                'verbose_name_plural': 'Group Members',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GroupRoles',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
                ('Role', models.CharField(db_column='Role', max_length=40)),
                ('Description', models.CharField(blank=True, db_column='Description', max_length=254, null=True)),
            ],
            options={
                'db_table': 'GroupRoles',
                'verbose_name': 'Group Role',
                'verbose_name_plural': 'Group Roles',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Groups',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
                ('Name', models.CharField(db_column='Name', max_length=40)),
                ('Description', models.CharField(blank=True, db_column='Description', max_length=254, null=True)),
            ],
            options={
                'db_table': 'Groups',
                'verbose_name': 'Group',
                'verbose_name_plural': 'Groups',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GroupTypes',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
                ('Type', models.CharField(db_column='Type', max_length=40)),
                ('Description', models.CharField(blank=True, db_column='Description', max_length=254, null=True)),
            ],
            options={
                'db_table': 'GroupTypes',
                'verbose_name': 'Group Type',
                'verbose_name_plural': 'Group Types',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='groups',
            name='GroupTypeId',
            field=models.ForeignKey(verbose_name='Group Types', to='api.GroupTypes', related_name='GRGroupTypeId'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupmembers',
            name='GroupId',
            field=models.ForeignKey(verbose_name='Group', to='api.Groups', related_name='GMGroupId'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupmembers',
            name='GroupRoleId',
            field=models.ForeignKey(verbose_name='Group Roles', to='api.GroupRoles', related_name='GMGroupRoleId'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='groupmembers',
            name='MembershipId',
            field=models.ForeignKey(verbose_name='Membership Id', to='api.Membership', related_name='GMMembershipId'),
            preserve_default=True,
        ),
        migrations.AlterModelOptions(
            name='familymembers',
            options={'verbose_name': 'Family Member', 'verbose_name_plural': 'Family Members'},
        ),
    ]
