# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20141204_0008'),
    ]

    operations = [
        migrations.CreateModel(
            name='Families',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
                ('Surname', models.CharField(db_column='Surname', max_length='40')),
                ('Description', models.CharField(db_column='Description', max_length='254')),
            ],
            options={
                'db_table': 'Families',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FamilyMembers',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
                ('FamilyId', models.ForeignKey(verbose_name='Family', related_name='FMFamilyId', to='api.Families')),
            ],
            options={
                'verbose_name': 'Family Member',
                'db_table': 'FamilyMembers',
                'verbose_name_plural': 'FamilyMembers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FamilyRoles',
            fields=[
                ('Id', models.AutoField(db_column='Id', serialize=False, primary_key=True)),
                ('Role', models.CharField(db_column='Role', max_length='40')),
                ('Description', models.CharField(db_column='Description', max_length='254')),
            ],
            options={
                'verbose_name': 'Family Role',
                'db_table': 'FamilyRoles',
                'verbose_name_plural': 'Family Roles',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='familymembers',
            name='FamilyRoleId',
            field=models.ForeignKey(verbose_name='Family Roles', related_name='FMFamilyRoleId', to='api.FamilyRoles'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='familymembers',
            name='MembershipId',
            field=models.ForeignKey(verbose_name='Membership Id', related_name='FMMembershipId', to='api.Membership'),
            preserve_default=True,
        ),
        migrations.AlterModelOptions(
            name='contactentries',
            options={'verbose_name_plural': 'Contact Entries', 'verbose_name': 'Contact Entry'},
        ),
        migrations.AlterModelOptions(
            name='contacttypes',
            options={'verbose_name_plural': 'Contact Types', 'verbose_name': 'Contact Type'},
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='ContactTypeId',
            field=models.ForeignKey(verbose_name='Contact Type Id', related_name='CEContactTypeId', to='api.ContactTypes'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactentries',
            name='MembershipId',
            field=models.ForeignKey(verbose_name='Membership Id', related_name='CEMembershipId', to='api.Membership'),
            preserve_default=True,
        ),
    ]
