from api.models import Membership, ContactEntries, ContactTypes
from api.models import Families, FamilyRoles, FamilyMembers
from api.models import Groups, GroupRoles, GroupTypes, GroupMembers

from rest_framework import serializers

class MembershipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Membership
        fields = ('LastName', 'FirstName', 'MiddleName', 'Birthday', 'Anniversary')
        