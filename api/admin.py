from django.contrib import admin
from api.models import Membership, ContactEntries, ContactTypes
from api.models import Families, FamilyRoles, FamilyMembers
from api.models import Groups, GroupRoles, GroupTypes, GroupMembers

class GroupMembersInline(admin.TabularInline):
    model = GroupMembers
    
class ContactEntriesInline(admin.TabularInline):
    model = ContactEntries
    
# Register your models here.
class MembershipAdmin( admin.ModelAdmin ):
    model = Membership
    list_display = ('FirstName', 'MiddleName', 'LastName', 'Birthday', 'Anniversary')
    search_fields = ('LastName', 'FirstName')
    
    inlines = [ContactEntriesInline]
    pass

admin.site.register(Membership, MembershipAdmin)

class ContactTypesAdmin( admin.ModelAdmin ):
    model = ContactTypes
    list_display = ('Type', 'Description')
    search_fields = ('Type',)
    pass

admin.site.register(ContactTypes, ContactTypesAdmin)

class ContactEntriesAdmin( admin.ModelAdmin ):
    model = ContactEntries
    list_display = ('MembershipId', 'ContactTypeId', 'Address', 'City', 'State', 'Zip', 'Phone', 'Email')
    list_filter = ('MembershipId', 'ContactTypeId', 'City', 'State', 'Zip')
    search_fields = ('City', 'State', 'Phone', 'Email')
    pass

admin.site.register(ContactEntries, ContactEntriesAdmin)

class FamiliesAdmin( admin.ModelAdmin ):
    model = Families
    list_display = ('Surname', 'Description')
    search_fields = ('Surname',)
    pass

admin.site.register(Families, FamiliesAdmin)

class FamilyRolesAdmin( admin.ModelAdmin ):
    model = FamilyRoles
    list_display = ('Role', 'Description')
    search_fields = ('Role',)
    pass

admin.site.register(FamilyRoles, FamilyRolesAdmin)

class FamilyMembersAdmin( admin.ModelAdmin ):
    model = FamilyMembers
    list_display = ('MembershipId', 'FamilyId', 'FamilyRoleId')
    list_filter = ('FamilyId', 'FamilyRoleId')
    search_fields = ('MembershipId', 'FamilyId', 'FamilyRoleId')
    pass

admin.site.register(FamilyMembers, FamilyMembersAdmin)

class GroupTypesAdmin( admin.ModelAdmin ):
    model = GroupTypes
    list_display = ('Type', 'Description')
    search_fields = ('Type',)
    pass

admin.site.register(GroupTypes, GroupTypesAdmin)

class GroupsAdmin( admin.ModelAdmin ):
    model = Groups
    list_display = ('Name', 'GroupTypeId', 'Description')
    list_filter = ('GroupTypeId',)
    search_fields = ('Name',)
    
    inlines = [GroupMembersInline]
    pass

admin.site.register(Groups, GroupsAdmin)

class GroupRolesAdmin( admin.ModelAdmin ):
    model = GroupRoles
    list_display = ('Role', 'Description')
    search_fields = ('Role',)
    pass

admin.site.register(GroupRoles, GroupRolesAdmin)

class GroupMembersAdmin( admin.ModelAdmin ):
    model = GroupMembers
    list_display = ('MembershipId', 'GroupId', 'GroupRoleId')
    list_filter = ('GroupId', 'GroupRoleId')
    search_fields = ('MembershipId', 'GroupId', 'GroupRoleId')
    pass

admin.site.register(GroupMembers, GroupMembersAdmin)
