from django.shortcuts import render

from api.models import Membership, ContactEntries, ContactTypes
from api.models import Families, FamilyRoles, FamilyMembers
from api.models import Groups, GroupRoles, GroupTypes, GroupMembers

from rest_framework import viewsets
from api.serializers import MembershipSerializer

# Create your views here.
class MembershipViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows members to be viewed or edited
    """
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
