from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from django.template import Context, loader

from api.models import Membership, ContactEntries, ContactTypes
from api.models import Families, FamilyRoles, FamilyMembers
from api.models import Groups, GroupRoles, GroupTypes, GroupMembers

from tcnanywhere.utilities import FormattedPhoneNumber, FormattedDate

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, portrait, landscape
from reportlab.lib.units import inch, cm
from reportlab.lib.colors import pink, black, red, blue, green

from datetime import datetime, timedelta, date

# Create your views here.
def homepage(request):
    membershipList = serializers.serialize('json', Membership.objects.all())
    contactEntriesList = serializers.serialize('json', ContactEntries.objects.all())
    template = loader.get_template('home.html')
    context = Context({'membershipList': membershipList, 'contactEntriesList': contactEntriesList})
    output = template.render(context)
    #membershipList = Membership.objects.all()
    #output = ", ".join([membership.FirstName + " " + membership.LastName for membership in membershipList])
    
    return HttpResponse(output)

def datagrid(request):
    membershipList = serializers.serialize('json', Membership.objects.all())
    contactEntriesList = serializers.serialize('json', ContactEntries.objects.all())
    template = loader.get_template('DataGrid.html')
    context = Context({'membershipList': membershipList, 'contactEntriesList': contactEntriesList})
    output = template.render(context)
    #membershipList = Membership.objects.all()
    #output = ", ".join([membership.FirstName + " " + membership.LastName for membership in membershipList])
    
    return HttpResponse(output)

def ssreport(request):
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    #response['Content-Disposition'] = 'attachment; filename="SundaySchoolReport.pdf"'

    # Create the PDF object, using the response object as its "file."
    pageCanvas = canvas.Canvas(response, pagesize=landscape(letter))

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    #p.drawString(100, 10, "Hello world.")
    #hello(p)
    designSundaySchoolReport(pageCanvas)
    
    # Close the PDF object cleanly, and we're done.
    pageCanvas.showPage()
    pageCanvas.save()
    
    return response

def designSundaySchoolReport(pageCanvas):
    quarterTitle = "Winter 2014 - 2015"
    startDate = date(2014, 12, 7)
    endDate = date(2015, 2, 22)

    # We handle the first class differently, since we need no newpage for the first class.
    firstClass = True

    sundaySchoolGroups = Groups.objects.filter(GroupTypeId__Type = "Sunday School Class").order_by("Name")
    for sundaySchoolClass in sundaySchoolGroups:
        print(sundaySchoolClass.GroupTypeId)
        numberOfWeeks = 0

        # We do not want a new page, which would be blank, for the first class.
        if firstClass:
            firstClass = False
        else:
            pageCanvas.showPage()
        
        textObject, numberOfWeeks = newPage(pageCanvas, startDate, endDate, quarterTitle, sundaySchoolClass.Name)
        ssClassMembers = GroupMembers.objects.filter(GroupId = sundaySchoolClass.Id).order_by("-GroupRoleId__Role", "MembershipId__LastName", "MembershipId__FirstName").select_related("Membership")
        
        for ssClassMember in ssClassMembers:
            member = ssClassMember.MembershipId
            print(ssClassMember.GroupRoleId)
            if str(ssClassMember.GroupRoleId) == "Teacher" or str(ssClassMember.GroupRoleId) == "Leader":
                print("This is a Teacher or Leader...")
                textObject.setFillColor(blue)
            elif str(ssClassMember.GroupRoleId) == "Prospect":
                print("This is a Prospect...")
                textObject.setFillColor(green)
            else:
                print("This is a Member...")
                textObject.setFillColor(black)
            
            #print("***** Member: " + str(ssClassMember.MembershipId) + " | Class: " + sundaySchoolClass.Name)
            #membershipList = Membership.objects.all()
            #for member in membershipList:
            textObject.textOut(member.FirstName + " " + member.LastName)
            if member.Birthday is not None:
                textObject.moveCursor(inch * 2, 0)
                textObject.textOut("Birthday: " + FormattedDate(member.Birthday))
                textObject.moveCursor(inch, 0)
            else:
                textObject.moveCursor(inch * 3, 0)

            for a in range(1,numberOfWeeks + 1):
                textObject.moveCursor(inch * .5, 0)
                textObject.textOut("_____")

            textObject.moveCursor(-(inch * .5 * (numberOfWeeks + 6)), 0)
            textObject.textLine(" ")

            try:
                contactEntries = ContactEntries.objects.get(MembershipId = member.Id, ContactTypeId__Type = "Home")
                print(str(contactEntries.ContactTypeId) + " | " + member.LastName)

                if contactEntries is not None:
                    textObject.textOut(contactEntries.Address)

                    if member.Anniversary is not None:
                        textObject.moveCursor(inch * 2, 0)
                        textObject.textOut("Anniversary: " + FormattedDate(member.Anniversary))
                        textObject.moveCursor(-inch *2, 0)
                        textObject.textLine(" ")
                    else:
                        textObject.moveCursor(0, 0)
                        textObject.textLine(" ")

                    textObject.textLine(contactEntries.City + ", " + contactEntries.State + " " + contactEntries.Zip)
                    textObject.textLine(FormattedPhoneNumber(contactEntries.Phone))
                    textObject = newPageIfNeeded(pageCanvas, textObject, startDate, endDate, quarterTitle, sundaySchoolClass.Name)
                else:
                    if member.Anniversary is not None:
                        textObject.moveCursor(inch * 2, 0)
                        textObject.textOut("Anniversary: " + FormattedDate(member.Anniversary))
                        textObject.moveCursor(-inch *2, 0)
                        textObject.textLine(" ")

            except:
                if member.Anniversary is not None:
                    textObject.moveCursor(inch * 2, 0)
                    textObject.textOut("Anniversary: " + FormattedDate(member.Anniversary))
                    textObject.moveCursor(-inch *2, 0)
                    textObject.textLine(" ")

            textObject.textLine(" ")
        
        pageCanvas.drawText(textObject)
        
def newPage(pageCanvas, startDate, endDate, quarterTitle, ssClassName):
    topOfPage = inch * 8
    leftOfPage = inch * 0.35

    pageCanvas.setStrokeColor(black)
    pageCanvas.setFont("Times-Roman", 10)

    pageText = pageCanvas.beginText()
    pageText.setTextOrigin(leftOfPage, topOfPage)

    #startDate = date(2014, 12, 7)
    #endDate = date(2015, 2, 22)

    pageText.textLine("Sunday School Rolls")
    pageText.textOut(quarterTitle)
    pageText.moveCursor(inch * 2, 0)
    pageText.textOut(ssClassName)
    pageText.moveCursor(inch * 1.5, 0)

    numberOfWeeks = 0
    currentDate = startDate
    while (currentDate <= endDate):
        #print("Current Date: " + FormattedDate(currentDate) + " | Start Date: " + FormattedDate(startDate) + " | End Date: " + FormattedDate(endDate))
        numberOfWeeks += 1
        pageText.textOut(FormattedDate(currentDate))
        pageText.moveCursor(inch * .5, 0)
        currentDate = currentDate + timedelta(days=7)

    #print(numberOfWeeks)
    pageText.moveCursor(-(inch * .5 * (numberOfWeeks + 7)), 0)
    pageText.textLine(" ")
    pageText.textLine(" ")
    pageText.textLine(" ")

    return (pageText, numberOfWeeks)

def newPageIfNeeded(pageCanvas, textObject, startDate, endDate, quarterTitle, ssClassName):
    print("Current Y: " + str(textObject.getY()))
    
    if textObject.getY() <= inch:
        print("New page...")
        pageCanvas.drawText(textObject)
        pageCanvas.showPage()
        
        topOfPage = inch * 8
        leftOfPage = inch * 0.35

        pageCanvas.setStrokeColor(black)
        pageCanvas.setFont("Times-Roman", 10)

        pageText = pageCanvas.beginText()
        pageText.setTextOrigin(leftOfPage, topOfPage)

        #startDate = date(2014, 12, 7)
        #endDate = date(2015, 2, 22)

        pageText.textLine("Sunday School Rolls")
        pageText.textOut(quarterTitle)
        pageText.moveCursor(inch * 2, 0)
        pageText.textOut(ssClassName)
        pageText.moveCursor(inch * 1.5, 0)

        numberOfWeeks = 0
        currentDate = startDate
        while (currentDate <= endDate):
            numberOfWeeks += 1
            pageText.textOut(FormattedDate(currentDate))
            pageText.moveCursor(inch * .5, 0)
            currentDate = currentDate + timedelta(days=7)

        print(numberOfWeeks)
        pageText.moveCursor(-(inch * .5 * (numberOfWeeks + 7)), 0)
        pageText.textLine(" ")
        pageText.textLine(" ")
        pageText.textLine(" ")

        return pageText
    else:
        return textObject